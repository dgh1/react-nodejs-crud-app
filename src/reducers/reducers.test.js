import constants from "../constants/constants";
import {reducers} from "../reducers/reducers";

describe("reducers", () => {

    it('handle default state', () => {

        const state = {};

        const action = reducers(state, {
            action: {
                type: constants.ACTION_DEFAULT_STATE,
                payload: {
                    action: constants.ACTION_DB_SELECTION,
                    dbSelection: "mongo"
                }
            }
        });

        const expected = {
            "payload": {
                "action": "action-default-state",
                "dbSelection": "elastic",
                "item": {"firstName": "", "id": null, "jobDescription": "", "lastName": ""},
                "login": false,
                "register": false
            }
        };

        expect(action).toEqual(expected);
    });

    it('db selection', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_DB_SELECTION,
            payload: {
                action: constants.ACTION_DB_SELECTION,
                dbSelection: "mongo"
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_DB_SELECTION,
                dbSelection: "mongo"
            }
        };

        expect(action).toEqual(expected);
    });


    it('add entry', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_ADD_ENTRY,
            payload: {
                action: constants.ACTION_ADD_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_ADD_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        };

        expect(action).toEqual(expected);
    });


    it('add entry', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_ADD_ENTRY,
            payload: {
                action: constants.ACTION_ADD_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_ADD_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        };

        expect(action).toEqual(expected);
    });


    it('update entry', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_UPDATE_ENTRY,
            payload: {
                action: constants.ACTION_UPDATE_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_UPDATE_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        };

        expect(action).toEqual(expected);
    });


    it('delete entry', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_DELETE_ENTRY,
            payload: {
                action: constants.ACTION_DELETE_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_DELETE_ENTRY,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        };

        expect(action).toEqual(expected);
    });



    it('update entry form', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_UPDATE_EDIT_FORM,
            payload: {
                action: constants.ACTION_UPDATE_EDIT_FORM,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_UPDATE_EDIT_FORM,
                item: {
                    firstName: "first",
                    id: "123456",
                    jobDescription: "janitor",
                    lastName: "lst"
                }
            }
        };

        expect(action).toEqual(expected);
    });


    it('login ', () => {

        const state = {};

        const action = reducers(state, {
            type: constants.ACTION_LOGIN,
            payload: {
                action: constants.ACTION_LOGIN,
                login: true
            }
        });

        const expected = {
            payload: {
                action: constants.ACTION_LOGIN,
                login: true
            }
        };

        expect(action).toEqual(expected);
    });



});
