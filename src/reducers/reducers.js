
//import merge from 'lodash.merge';
import { combineReducers } from 'redux';
import constants from '../constants/constants';

export const payload = (state , action) => {

    switch (action.type) {

        case constants.ACTION_DB_SELECTION:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_ADD_ENTRY:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_UPDATE_ENTRY:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_DELETE_ENTRY:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_UPDATE_EDIT_FORM:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_LOGIN:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        case constants.ACTION_REGISTER:
            action.payload.action = action.type;
            return Object.assign({}, state, action.payload);

        default:
            return {
                item: {
                    id: null,
                    firstName: '',
                    lastName: '',
                    jobDescription: ''
                },
                action: constants.ACTION_DEFAULT_STATE,
                dbSelection: constants.DB_SELECTION_ELASTIC,
                login: false,
                register: false
            };
    }
};


// let payload = merge({}, state);
// payload.lastAction = payload.currentAction;
// payload.currentAction = action;
// return payload;

export const reducers = combineReducers({
    payload,
});
