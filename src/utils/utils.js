/**
 *
 * Usage:  import Utils from '../utils/Utils.js';
 *
 */

const Utils = {

    formatDate(date) {
        const hours = date.getHours().toString().padStart(2, "0");
        const minutes = date.getMinutes().toString().padStart(2, "0");
        const seconds = date.getSeconds().toString().padStart(2, "0");
        const msecs = date.getMilliseconds().toString().padStart(3, "0");

        return (hours + ":" + minutes + ":" + seconds + ":" + msecs);
    }
};


export default Utils;























