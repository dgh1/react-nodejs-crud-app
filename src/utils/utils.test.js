import Utils from "./utils/"

describe("utils", () => {


    it('formatted time ', () => {

        const date = new Date(2001, 2, 3, 10, 30, 45, 234);
        const formatted = Utils.formatDate(date);
        const expected = "10:30:45:234";

        expect(formatted).toEqual(expected);
    });


});

