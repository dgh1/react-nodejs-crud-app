import {axios} from "../common/common";


const RegisterUser = (config) => {
    axios({
        method: 'post',
        url: '/credentials/register',
        data: {
            userName: config.userName,
            password:  config.password
        }
    }).then(function (response) {
        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });

    }).catch(function ( rsp) {
        config.callback({
            status:  rsp.response.data.reason

        });

    }).then(function () {
        //console.log("always executed.....");
    });
};


const LoginUser = (config) => {
    axios({
        method: 'post',
        url: '/credentials/login',
        data: {
            userName: config.userName,
            password:  config.password
        }
    }).then(function (response) {
        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });

    }).catch(function ( rsp) {

        config.callback({
            status:  rsp.response.status,
            statusText:  rsp.response.data.reason.error

        });

    }).then(function () {
        //console.log("always executed.....");
    });
};


const LogoutUser = (config) => {
    axios({
        method: 'post',
        url: '/credentials/logout',
        data: {
            userName: config.userName
        }
    }).then(function (response) {
        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });

    }).catch(function ( rsp) {
        config.callback({
            status:  rsp.response.data.reason

        });

    }).then(function () {
        //console.log("always executed.....");
    });
};


const GetAllData = (config) => {
    axios({
        method: 'get',
        url: '/accounts/person/' + config.db,
    }).then(function (response) {
        let data = response.data;
        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText,
            data:   data
        });

    }).catch(function ( error) {
        const errorCodes = getErrorCodes(error);
        config.callback({
            status:  errorCodes.status,
            statusText:  errorCodes.statusText
        });

    }).then(function () {
        //console.log("always executed.....");
    });
};

const AddDocument = (config) => {

    axios({
        method: "POST",
        url: '/accounts/person/' + config.db ,
        data: {
            firstName: config.firstName,
            lastName:  config.lastName,
            jobDescription: config.jobDescription
        }
    }).then(function (response) {

        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });
    }).catch(function (error) {
        const errorCodes = getErrorCodes(error);
        config.callback({
            status:  errorCodes.status,
            statusText:  errorCodes.statusText
        });

    }).then(function () {
        //console.log("always executed.....");
    });

};

const DeleteDocument = (config) => {

    axios({
        method: 'DELETE',
        url: '/accounts/person/' + config.db  + "/" + config.id,
    }).then(function (response) {

        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });

    }).catch(function (error) {
        const errorCodes = getErrorCodes(error);
        config.callback({
            status:  errorCodes.status,
            statusText:  errorCodes.statusText
        });
    }).then(function () {
        //console.log("always executed.....");
    });

};

const UpdateDocument = (config) => {
    axios({
        method: "PUT",
        url: '/accounts/person/' + config.db + "/" + config.id,
        data: {
            id: config.id,
            firstName: config.firstName,
            lastName:  config.lastName,
            jobDescription: config.jobDescription
        }
    }).then(function (response) {

        config.callback({
            status:  response.request.status,
            statusText:  response.request.statusText
        });

    }).catch(function (error) {
        const errorCodes = getErrorCodes(error);
        config.callback({
            status:  errorCodes.status,
            statusText:  errorCodes.statusText
        });

    }).then(function () {
        //console.log("always executed.....");
    });
};

const getErrorCodes = (error) =>{
    let status = "";
    let statusText = "";
    if (error.response) {
        status = error.response.request.status;
        statusText = error.response.request.statusText;
    }
    else {
        status = "503";
        statusText = "Service Unavailable";
    }
    return {
        status: status,
        statusText: statusText
    }
};

export default {
    GetAllData,
    AddDocument,
    DeleteDocument,
    UpdateDocument,
    RegisterUser,
    LoginUser,
    LogoutUser

}