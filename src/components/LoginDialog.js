import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';
import constants from '../constants/constants'
import './LoginDialog.css';
import Api from "../api/Api";

const styles = {
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1,
    }

};


const LoginDialog = class LoginDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            login: false,
            name: ""
        };

        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleChange = (name) => event => {
        this.setState({[name]: event.target.value});
        document.getElementById("login-error-msg").innerText = "";
    };

    handleClose = (event, reason) => {
        let isLoggedIn = false;

        if (reason) {
            this.setState({open: isLoggedIn});
        }
        else {
            let action = event.target.innerText;

            if (action === constants.CANCEL) {
                this.setState({open: isLoggedIn});
            }
            else if (action === constants.SIGN_IN) {
                let userEntry = document.getElementById("user").value.trim();
                let passwordEntry = document.getElementById("password").value.trim();
                if (userEntry === "" || passwordEntry === "") {
                    document.getElementById("login-error-msg").innerText = "Both fields are required";
                    return;
                }
                else {
                    //isLoggedIn = true;
                    let me = this;
                    Api.LoginUser({
                        callback: (rsp)  => {
                            if (rsp.status === 200) {
                                isLoggedIn = true;
                                me.setState({open: false});
                                this.props.config.cb(isLoggedIn,userEntry);
                            }
                            else {
                                me.setState({open: true});
                                document.getElementById("login-error-msg").innerText = ("Error: " + rsp.statusText );
                            }
                        },
                        userName: userEntry,
                        password: passwordEntry
                    });

                }
            }
        }
        //this.props.config.cb(isLoggedIn);
    };


    render() {
        //const {classes} = this.props;
        return (
            <div>
                <Button color="inherit" onClick={this.handleClickOpen}>
                    Login
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Sign In</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter below credentials
                        </DialogContentText>
                        <TextField
                            margin="dense"
                            id="user"
                            name="user"
                            label="User Name"
                            type="text"
                            required="true"
                            onChange={this.handleChange('name')}
                            fullWidth
                        />
                        <TextField
                            margin="dense"
                            id="password"
                            label="Password"
                            name="password"
                            type="password"
                            required="true"
                            onChange={this.handleChange('name')}
                            fullWidth
                        />

                    </DialogContent>
                    <span className="login-error-msg" id="login-error-msg"></span>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Sign In
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
};


LoginDialog.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoginDialog);