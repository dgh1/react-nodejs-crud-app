import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';
import constants from '../constants/constants'
import './LoginDialog.css';
import Api from "../api/Api";

const styles = {
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1,
    }

};


const RegisterUserDialog = class RegisterUserDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            register: false,
            name: ""
        };

        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);

    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleChange = (name) => event => {
        this.setState({[name]: event.target.value});
        document.getElementById("login-error-msg").innerText = "";
    };

    handleClose = (event, reason) => {
        let isRegistered = false;

        if (reason) {
            isRegistered = false;
            this.setState({open: false});
        }
        else {
            let action = event.target.innerText;

            if (action === constants.CANCEL) {
                isRegistered = false;
                this.setState({open: false});
            }
            else if (action === constants.REGISTER) {
                let userEntry = document.getElementById("user").value.trim();
                let passwordEntry = document.getElementById("password").value.trim();
                let reenterPasswordEntry = document.getElementById("reenter-password").value.trim();

                if (userEntry === "" || passwordEntry === ""  || reenterPasswordEntry === "") {
                    document.getElementById("login-error-msg").innerText = "All fields are required";
                    return;
                }
                if (passwordEntry !== reenterPasswordEntry) {
                    document.getElementById("login-error-msg").innerText = "Passwords did not match";
                    return;
                }
                else {
                    let me = this;
                    Api.RegisterUser({
                        callback: (rsp)  => {
                            if (rsp.status === 200) {
                                isRegistered = true;
                                me.setState({open: false});
                                this.props.config.cb(isRegistered);
                            }
                            else {
                                document.getElementById("login-error-msg").innerText = ("Error: " + rsp.status );
                            }
                        },
                        userName: userEntry,
                        password: passwordEntry
                    });

                }
            }
        }
    };


    render() {
        //const {classes} = this.props;
        return (
            <div>
                <Button color="inherit" onClick={this.handleClickOpen}>
                    Register
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Register</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter below credentials
                        </DialogContentText>
                        <TextField
                            margin="dense"
                            id="user"
                            name="user"
                            label="User Name"
                            type="text"
                            required="true"
                            onChange={this.handleChange('name')}
                            fullWidth
                        />
                        <TextField
                            margin="dense"
                            id="password"
                            label="Password"
                            name="password"
                            type="password"
                            required="true"
                            onChange={this.handleChange('name')}
                            fullWidth
                        />
                        <TextField
                            margin="dense"
                            id="reenter-password"
                            label="re-enter Password"
                            name="password"
                            type="password"
                            required="true"
                            onChange={this.handleChange('name')}
                            fullWidth
                        />

                    </DialogContent>
                    <span className="login-error-msg" id="login-error-msg"></span>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Register
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
};


RegisterUserDialog.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RegisterUserDialog);