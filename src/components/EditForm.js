import React from 'react';
import './EditForm.css';
import constants from "../constants/constants";
import {emitter} from "../common/common";
import Api from '../api/Api';
//import {connect} from "react-redux";

import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({

    button: {
        margin: theme.spacing.unit,
        //margin: 5
    },
    input: {
        display: 'none',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300
    },
    dense: {
        marginTop: 3

    },
});

let dbSelection = "";

const EditForm = class EditForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            firstName: '',
            lastName: '',
            jobDescription: ''
        };

        dbSelection = props.config.payload.dbSelection;

        this.deleteDocument = this.deleteDocument.bind(this);
        this.addDocument = this.addDocument.bind(this);
        this.updateDocument = this.updateDocument.bind(this);
        this.clearDataEntry = this.clearDataEntry.bind(this);

        let me = this;

        emitter.on(constants.REDUX_STATE_CHANGE, function (payload) {
            if (payload.action === constants.ACTION_UPDATE_EDIT_FORM) {
                me.setState((state, props) => ({
                    id: payload.item.id,
                    firstName: payload.item.firstName,
                    lastName: payload.item.lastName,
                    jobDescription: payload.item.jobDescription
                }));
                me.clearErrorField();
            }
            else if (payload.action === constants.ACTION_DB_SELECTION) {
                dbSelection = payload.dbSelection;
            }
        });

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    setErrorField = (error) => {
        document.getElementById('error-display').innerText = error;
    };

    clearErrorField = () => {
        document.getElementById('error-display').innerText = "";
    };

    deleteDocument() {
        if (!this.validateFrom(this.getFormData())) {
            return false;
        }

        let me = this;
        Api.DeleteDocument({
            callback: (status) => {
                if (status.status === 200) {
                    me.stateDeleteEntry();
                    me.clearDataEntry();
                }
                else {
                    me.setErrorField("Status: " + status.status + "  " + status.statusText)
                }
            },
            db: dbSelection,
            id: this.state.id,
        });
    };

    addDocument() {
        let data = this.getFormData();

        if (!this.validateFrom(data)) {
            return false;
        }

        let me = this;
        Api.AddDocument({
            callback: (status)  => {
                if (status.status === 200) {
                    me.stateAddEntry();
                    me.clearDataEntry();
                }
                else {
                    me.setErrorField("Status: " + status.status + "  " + status.statusText)
                }
            },
            db: dbSelection,
            firstName: data.firstName,
            lastName: data.lastName,
            jobDescription: data.jobDescription
        });

    }

    updateDocument() {
        let data = this.getFormData();

        if (!this.validateFrom(data)) {
            return false;
        }

        let me = this;
        Api.UpdateDocument({
            callback:  (status) => {
                if (status.status === 200) {
                    me.stateUpdateEntry();
                    me.clearDataEntry();
                }
                else {
                    me.setErrorField("Status: " + status.status + "  " + status.statusText)
                }
            },
            db: dbSelection,
            id: this.state.id,
            firstName: data.firstName,
            lastName: data.lastName,
            jobDescription: data.jobDescription
        });
    }

    stateUpdateEntry() {
        // redux.dispatch
        this.props.config.updateEntry({
            item: {
                id: this.state.id,
                firstName: this.state.firstName.trim(),
                lastName: this.state.lastName.trim(),
                jobDescription: this.state.jobDescription.trim()
            }
        })
    }

    stateAddEntry() {
        // redux.dispatch
        this.props.config.addEntry({
            item: {
                id: this.state.id,
                firstName: this.state.firstName.trim(),
                lastName: this.state.lastName.trim(),
                jobDescription: this.state.jobDescription.trim()
            }
        })
    }

    stateDeleteEntry() {
        // redux.dispatch
        this.props.config.deleteEntry({
            item: {
                id: this.state.id,
                firstName: this.state.firstName.trim(),
                lastName: this.state.lastName.trim(),
                jobDescription: this.state.jobDescription.trim()
            }
        })
    }

    getFormData() {
        return {
            id: this.state,
            firstName: this.state.firstName.trim(),
            lastName: this.state.lastName.trim(),
            jobDescription: this.state.jobDescription.trim()
        }
    }

    validateFrom(data) {
        if (data.firstName === "" || data.lastName === "" || data.jobDescription === "") {
            this.setErrorField("All fields are required");
            return false;
        }
        return true;
    }

    clearDataEntry() {
        this.setState((state, props) => ({
            id: "",
            firstName: "",
            lastName: "",
            jobDescription: ""
        }));

        this.clearErrorField();
    }

    render() {
        const {classes} = this.props;
        return (
            <div>
                <div className="center-edit-form"><FormLabel> Edit Form</FormLabel></div>
                <div>
                    <form className={classes.container} noValidate autoComplete="off">
                        <TextField
                            id="first-name"
                            label="First Name"
                            className={classNames(classes.textField, classes.dense)}
                            margin="dense"
                            value={this.state.firstName}
                            onChange={this.handleChange('firstName')}
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            id="last-name"
                            label="Last Name"
                            className={classNames(classes.textField, classes.dense)}
                            margin="dense"
                            value={this.state.lastName}
                            onChange={this.handleChange('lastName')}
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            id="job-description"
                            label="Job Description"
                            className={classNames(classes.textField, classes.dense)}
                            margin="dense"
                            value={this.state.jobDescription}
                            onChange={this.handleChange('jobDescription')}
                            variant="outlined"
                        />
                    </form>

                    <div>
                        <Button variant="outlined" size="small" className={classes.button}
                                onClick={this.deleteDocument}>Delete</Button>
                        <Button variant="outlined" size="small" className={classes.button}
                                onClick={this.addDocument}>Add</Button>
                        <Button variant="outlined" size="small" className={classes.button}
                                onClick={this.updateDocument}>Update</Button>
                        <Button variant="outlined" size="small" className={classes.button}
                                onClick={this.clearDataEntry}>Clear</Button>
                    </div>
                    <Typography color="error" id="error-display"> </Typography>
                </div>
            </div>
        );
    }
};

//export default EditForm;


EditForm.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(EditForm);

