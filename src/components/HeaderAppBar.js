import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
//import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
//import constants from "../constants/constants";
import LoginDialog from "../components/LoginDialog"
import RegisterUserDialog from "../components/RegisterUserDialog"
import Api from "../api/Api";

const styles = {
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -10,
        marginRight: 20,
    },
};


const HeaderAppBar = class HeaderAppBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            isLoggedIn: false,
            isRegistered: false,
            open: false,
            user: ""
        };

        //this.handleLoginClick = this.handleLoginClick.bind(this);

        this.handleClose = this.handleClose.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.processCloseProfile = this.processCloseProfile.bind(this);
        this.processCloseAccount = this.processCloseAccount.bind(this);
        this.handleCloseLogout = this.handleCloseLogout.bind(this);
        this.loginCallBack = this.loginCallBack.bind(this);
        this.registerCallBack = this.registerCallBack.bind(this);
    }

    // when menu open and click outside of mentu
    handleClose = () => {
        this.setState({anchorEl: null});
    };

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    processCloseProfile = (event) => {
        this.setState({anchorEl: null});
    };

    processCloseAccount = (event) => {
        this.setState({anchorEl: null});
    };

    handleCloseLogout = (event) => {
        this.setState({
            anchorEl: null,
            isLoggedIn: false
        });

        let me = this;
        Api.LogoutUser({
            callback: (rsp)  => {
                if (rsp.status === 200) {
                    me.isLoggedIn = false;
                     me.setState({open: false});
                    this.props.config.login({
                        login: false,
                        register: true,
                        user: me.user
                    });

                }
                else {
                    document.getElementById("login-error-msg").innerText = ("Error: " + rsp.status );
                }
            },
            userName: me.user

        });

    };

    loginCallBack = (login, user) => {
        this.setState({
            anchorEl: null,
            isLoggedIn: login,
            isRegistered: true,
            user: user
        });

        this.props.config.login({
            login: login,
            register: true,
            user: user
        });

        this.user = user;
    };

    registerCallBack = (register) => {
         this.setState({
             anchorEl: null,
             isRegistered: register
         });

        this.props.config.register({
            register: register
        });
    };

    render() {
        const {anchorEl} = this.state;
        const {classes} = this.props;
        const isLoggedIn = this.state.isLoggedIn;
        const isRegistered = this.state.isRegistered;

        let buttonLogin;
        let buttonRegister;
        let menu;
        let greetings;

        if (!isLoggedIn  && !isRegistered) {
            buttonLogin = <LoginDialog color="inherit"  config={{cb: this.loginCallBack} } >Login</LoginDialog>
            buttonRegister = <RegisterUserDialog color="inherit"  config={{cb: this.registerCallBack} } >Register</RegisterUserDialog>
        }
        else if (isRegistered && !isLoggedIn) {
            buttonLogin = <LoginDialog color="inherit"  config={{cb: this.loginCallBack} } >Login</LoginDialog>
            buttonRegister = null;
        }
        else if (isLoggedIn) {
            menu =  <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}
            >
                <MenuItem onClick={this.processCloseProfile}>Profile</MenuItem>
                <MenuItem onClick={this.processCloseAccount}>My account</MenuItem>
                <MenuItem onClick={this.handleCloseLogout}>Logout</MenuItem>
            </Menu>

            greetings = <Typography color="inherit"> Welcome back {this.state.user} </Typography>
        }

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar variant="dense">

                        <IconButton
                            className={classes.menuButton}
                            color="inherit" aria-label="Menu"
                            aria-owns={anchorEl ? 'simple-menu' : undefined}
                            aria-haspopup="true"
                            onClick={this.handleClick}
                        >
                        <MenuIcon/>
                        </IconButton>

                        {menu}

                        <Typography variant="h6" color="inherit" className={classes.grow}>
                        </Typography>

                        {buttonLogin}
                        {buttonRegister}
                        {greetings}
                    </Toolbar>

                </AppBar>
            </div>
        );
    }
};

HeaderAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HeaderAppBar);