import React from 'react';
import './DocsTable.css';

import {AgGridReact} from "ag-grid-react";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import {emitter} from "../common/common";

import constants from "../constants/constants";

import Api from '../api/Api';

import Radio from '@material-ui/core/Radio';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
//import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';

import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';


let dbSelection = null;


const styles = theme => ({
    root: {
        display: 'flex',
        //display: 'inline-block',
    },
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
});

const DocsTable = class DocsTable extends React.Component {

    constructor(props) {
        super(props);

        dbSelection = props.config.payload.dbSelection;

        this.state = {
            selected: dbSelection,   //constants.DB_SELECTION_MONGO, //props.db,
            columnDefs: [
                {headerName: "FirstName", field: "firstName", width: 100, sortable: true},
                {headerName: "LastName", field: "lastName", width: 100, sortable: true},
                {headerName: "Job Description", field: "jobDescription", width: 250, sortable: true}
            ],
            rowData: []
        };


        let me = this;
        emitter.on(constants.REDUX_STATE_CHANGE, (payload) => {
            let action = payload.action;
            if (action === constants.ACTION_ADD_ENTRY || action === constants.ACTION_DELETE_ENTRY || action === constants.ACTION_UPDATE_ENTRY) {
                Api.GetAllData({
                    db: dbSelection,
                    callback: function (config) {
                        if (config.status === 200) {
                            me.updateTable(config);
                        }
                        else {
                            me.setErrorField("Status: " + config.status + "  " + config.statusText);
                        }
                    }
                })
            }

        });

    }

    componentDidMount() {
        // fetch('/accounts/person/' + this.state.selected)
        //     .then(result => result.json())
        //     .then((rowData) => {
        //             this.setState({rowData});
        //             this.displayRecordCnt(rowData.length);
        //         }
        //     ).catch(error => console.error(error)
        // );

        let me = this;
        Api.GetAllData({
            db: this.state.selected,
            callback: function (config) {
                if (config.status === 200) {
                    me.updateTable(config);
                }
                else {
                    me.setErrorField("Status: " + config.status + "  " + config.statusText);
                }
            }
        })
    }

    updateTable(config) {
        let rowData = config.data;
        this.setState({rowData});
        this.displayRecordCnt(rowData.length);
    }

    dbSelectionChange = (e) => {
        const {value} = e.target;

        this.setState((state, props) => ({
            selected: value
        }));

        dbSelection = value;

        // redux.dispatch
        this.props.config.dbSelection({
            dbSelection: value
        });

        let me = this;
        Api.GetAllData({
            db: dbSelection,
            callback: function (config) {
                if (config.status === 200) {
                    me.updateTable(config);
                }
                else {
                    me.setErrorField("Status: " + config.status + "  " + config.statusText);
                }
            }
        })

    };

    onGridSelectionChanged() {
        let rowData = this.gridApi.getSelectedRows()[0];

        // redux.dispatch
        this.props.config.updateEditForm({
            item: {
                id: rowData.id,
                firstName: rowData.firstName,
                lastName: rowData.lastName,
                jobDescription: rowData.jobDescription
            }
        });
    }

    displayRecordCnt(cnt) {
        document.getElementById('table-row-count').innerText = "";
        document.getElementById('table-row-count').innerText = "Record Count: " + cnt;
    }

     setErrorField = (error) => {
         document.getElementById('table-row-count').innerText = "";
         document.getElementById('table-error-display').innerText = error;
     };

    render() {
        const {classes} = this.props;

        return (
            <div>
                <div>
                    <RadioGroup
                        row
                        name="dbSelection"
                        className={classes.group}
                        value={this.state.selected}
                        onChange={this.dbSelectionChange}
                    >
                        <FormControlLabel
                            value="elastic"
                            control={<Radio color="primary"/>}
                            label="Elastic Search"
                            icon={<RadioButtonUncheckedIcon fontSize="small"/>}
                            labelPlacement="end"
                        />
                        <FormControlLabel
                            value="mongo"
                            control={<Radio color="primary"/>}
                            label="MongoDB"
                            icon={<RadioButtonUncheckedIcon fontSize="small"/>}
                            labelPlacement="end"
                        />
                    </RadioGroup>
                </div>

                <div
                    className="ag-theme-balham"
                    style={{
                        height: '175px',
                        width: '400px',
                        margin: '3px'
                    }}
                >
                    <AgGridReact
                        onGridReady={(params) => {
                            this.gridApi = params.api
                        }
                        }
                        onSelectionChanged={this.onGridSelectionChanged.bind(this)}
                        rowSelection="single"
                        columnDefs={this.state.columnDefs}
                        rowData={this.state.rowData}>
                    </AgGridReact>
                </div>

                {/* <FormLabel variant="subtitle1" id="table-row-count"> </FormLabel> */}
                <Typography color="primary" id="table-row-count" >    </Typography>
                <Typography color="error" id="table-error-display" >    </Typography>

            </div>
        );
    }

    // onButtonClickRefresh = (e) => {
    //     let me = this;
    //     Api.GetAllData({
    //         db: dbSelection,
    //         callback: function (config) {
    //             if (config.status === 200) {
    //                 me.updateTable(config);
    //             }
    //             else {
    //                 me.setErrorField("Status: " + config.status + "  " + config.statusText);
    //             }
    //         }
    //     })
    // };

};

//export default DocsTable;


DocsTable.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(DocsTable);


// const mapStateToProps = function (state) {
//     let action = state.payload.action;
//     if (action === constants.ADD_ENTRY || action === constants.DELETE_ENTRY || action === constants.UPDATE_ENTRY) {
//         setTimeout(function(){
//             emitter.emit(constants.UPDATE_DATA_TABLE, {});
//         }, 1000);
//     }
// };
//
// export default connect(mapStateToProps)(DocsTable);




