
import React  from 'react';
import './App.css';
import './layout.css';
import DocsTable from './components/DocsTable';
import EditForm from './components/EditForm';
import HeaderAppBar from './components/HeaderAppBar';

export class App extends React.Component {
    render() {
        return (
            <div className="wrapper">
                <header>  <HeaderAppBar config={this.props}/> </header>
                <main>
                    <div className="box-style-no-border  box1 ">
                        <div className="container">
                            {/*    --- Column  1 --- */}
                            <div className="column column-one">
                                <fieldset className="field-set">
                                    <div style={{ height: '250px', width: '400px', margin: '3px' }}>
                                        <div>
                                            <DocsTable config={this.props}/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            {/*    --- Column  2 --- */}
                            <div className="column column-two">
                                <fieldset className="field-set">
                                    <EditForm  config={this.props}/>
                                </fieldset>
                            </div>
                            {/*    --- Column  3 ---
                            <div className="column column-three">
                            </div>
                            */}
                        </div>
                    </div>
                </main>
                <footer> Version 1.0.20 </footer>
            </div>
        );
    }
}

//export default App;
