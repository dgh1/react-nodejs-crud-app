
const EventEmitter = require('events');
export const emitter = new EventEmitter();

const ax = require('axios');

/**
 * To run in HTTPS mode change below baseURL property value:
 *
 * from http to https
 *
 */
export const axios = ax.create({
    baseURL: 'http://localhost:3000/'
});


