

const DB_SELECTION_MONGO = "mongo";
const DB_SELECTION_ELASTIC = "elastic";
const REDUX_STATE_CHANGE = "redux-state-change";

// redux actions
const ACTION_ADD_ENTRY = "action-add-entry";
const ACTION_UPDATE_ENTRY = "action-update-entry";
const ACTION_DELETE_ENTRY = "action-delete-entry";
const ACTION_UPDATE_EDIT_FORM = "action-update-edit-form";
const ACTION_DB_SELECTION = "action-database-selection";
const ACTION_DEFAULT_STATE = 'action-default-state';
const ACTION_LOGIN = 'action-login';
const ACTION_REGISTER = 'action-register';

let SIGN_IN = "SIGN IN";
let CANCEL = "CANCEL";
let REGISTER = "REGISTER";


export default {
    ACTION_DB_SELECTION,
    ACTION_ADD_ENTRY,
    ACTION_UPDATE_ENTRY,
    ACTION_DELETE_ENTRY,
    ACTION_DEFAULT_STATE,
    ACTION_UPDATE_EDIT_FORM,
    DB_SELECTION_MONGO,
    DB_SELECTION_ELASTIC,
    REDUX_STATE_CHANGE,
    ACTION_LOGIN,
    ACTION_REGISTER,
    SIGN_IN,
    CANCEL,
    REGISTER



}


