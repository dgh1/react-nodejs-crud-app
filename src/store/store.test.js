import constants from "../constants/constants";
import {store} from './store'

import {
    dbSelection
} from "../actions/actions";


describe("actions", () => {


    it('login ', () => {

        beforeEach(() => { // Runs before each test in the suite
            store.clearActions();
        });

        const expectedResponse  =  {
            action: constants.ACTION_DB_SELECTION,
            dbSelection: constants.DB_SELECTION_MONGO,
            item: {
                firstName: "",
                id: null,
                jobDescription: "",
                lastName: ""
            },
            login: false,
            register: false
        };

        store.dispatch(dbSelection({
                action: constants.ACTION_DB_SELECTION,
                dbSelection: constants.DB_SELECTION_MONGO
        }));

        const actualResponse = store.getState().payload;

        expect(expectedResponse).toEqual(actualResponse);

    });


});

