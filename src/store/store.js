
import {  createStore } from 'redux';
import {reducers} from "../reducers/reducers";
import {emitter} from "../common/common";
import constants from "../constants/constants";
import Utils from "../utils/utils";

export function configureStore(initialState = {}) {
    return  createStore(reducers, initialState);
}

export const store = configureStore();

window.stateLogger = [];

/**
 * Broadcast store changes so interested parties
 * can take any state change actions.
 *
 * show to console any store changes including
 * the initial state
 */

const render = () => {
    let payload =  store.getState().payload;
    const maxLoggerLength = 30;

    if (window.stateLogger.length > maxLoggerLength) {
        window.stateLogger.shift();
    }
    window.stateLogger.push( Object.assign({}, { ts: Utils.formatDate(new Date())}, payload));
    emitter.emit(constants.REDUX_STATE_CHANGE, payload);
};

store.subscribe(render);
// show the initial store state on startup
render();

