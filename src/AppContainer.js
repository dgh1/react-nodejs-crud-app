
import {connect} from "react-redux";

import {
    addEntry,
    updateEntry,
    deleteEntry,
    updateEditForm,
    dbSelection,
    login,
    register

} from "./actions/actions";

import {App} from "./App";

const mapStateToProps = state => ({
    payload: state.payload,
});

const mapDispatchToProps = {
    addEntry,
    updateEntry,
    deleteEntry,
    updateEditForm,
    dbSelection,
    login,
    register
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppContainer;
