import constants from "../constants/constants";
import {
    addEntry,
    updateEntry,
    deleteEntry,
    updateEditForm,
    dbSelection,
    login

} from "../actions/actions";

describe("actions", () => {



    it('db selection ', () => {

        const action = {
            action: constants.ACTION_DB_SELECTION,
            dbSelection: constants.DB_SELECTION_MONGO
        };

        const actualResponse = dbSelection(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_DB_SELECTION,
                dbSelection: "mongo"
            },
            type: constants.ACTION_DB_SELECTION
        };

        expect(expectedResponse).toEqual(actualResponse);

     });

    it('add entry ', () => {

        const action = {
            action: constants.ACTION_ADD_ENTRY,
            item: {
                firstName: "first",
                id: "12345",
                jobDescription: "janitor",
                lastName: "last"
            }
        };

        const actualResponse = addEntry(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_ADD_ENTRY,
                item: {
                    firstName: "first",
                    id: "12345",
                    jobDescription: "janitor",
                    lastName: "last"
                }
            },
            type: constants.ACTION_ADD_ENTRY
        };

        expect(expectedResponse).toEqual(actualResponse);

    });

    it('update entry ', () => {

        const action = {
            action: constants.ACTION_UPDATE_ENTRY,
            item: {
                firstName: "first",
                id: "12345",
                jobDescription: "janitor",
                lastName: "last"
            }
        };

        const actualResponse = updateEntry(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_UPDATE_ENTRY,
                item: {
                    firstName: "first",
                    id: "12345",
                    jobDescription: "janitor",
                    lastName: "last"
                }
            },
            type: constants.ACTION_UPDATE_ENTRY
        };

        expect(expectedResponse).toEqual(actualResponse);

    });


    it('delete entry ', () => {

        const action = {
            action: constants.ACTION_DELETE_ENTRY,
            item: {
                firstName: "first",
                id: "12345",
                jobDescription: "janitor",
                lastName: "last"
            }
        };

        const actualResponse = deleteEntry(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_DELETE_ENTRY,
                item: {
                    firstName: "first",
                    id: "12345",
                    jobDescription: "janitor",
                    lastName: "last"
                }
            },
            type: constants.ACTION_DELETE_ENTRY
        };

        expect(expectedResponse).toEqual(actualResponse);

    });


    it('update edit form ', () => {

        const action = {
            action: constants.ACTION_UPDATE_EDIT_FORM,
            item: {
                firstName: "first",
                id: "12345",
                jobDescription: "janitor",
                lastName: "last"
            }
        };

        const actualResponse = updateEditForm(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_UPDATE_EDIT_FORM,
                item: {
                    firstName: "first",
                    id: "12345",
                    jobDescription: "janitor",
                    lastName: "last"
                }
            },
            type: constants.ACTION_UPDATE_EDIT_FORM
        };

        expect(expectedResponse).toEqual(actualResponse);

    });


    it('login ', () => {

        const action = {
            action: constants.ACTION_LOGIN,
            login: true
        };

        const actualResponse = login(action);

        const expectedResponse =  {
            payload: {
                action: constants.ACTION_LOGIN,
                login: true
            },
            type: constants.ACTION_LOGIN
        };

        expect(expectedResponse).toEqual(actualResponse);

    });






});

