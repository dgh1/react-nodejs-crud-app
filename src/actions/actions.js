
import constants from '../constants/constants';

export const addEntry = (payload) => ({
    type: constants.ACTION_ADD_ENTRY,
    payload: payload
});

export const updateEntry = (payload) => ({
    type: constants.ACTION_UPDATE_ENTRY,
    payload: payload
});

export const deleteEntry = (payload) => ({
    type: constants.ACTION_DELETE_ENTRY,
    payload: payload
});

export const updateEditForm = (payload) => ({
    type: constants.ACTION_UPDATE_EDIT_FORM,
    payload: payload
});

export const dbSelection = (payload) => ({
    type: constants.ACTION_DB_SELECTION,
    payload: payload
});

export const login = (payload) => ({
    type: constants.ACTION_LOGIN,
    payload: payload
});

export const register = (payload) => ({
    type: constants.ACTION_REGISTER,
    payload: payload
});

