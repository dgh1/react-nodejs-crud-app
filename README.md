This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


This is a react CRUD application with react-redux. Some of the react components used
are from google's material-ui.  The application depends on another application _(back-end)_
to support the CRUD api calls. The _(back-end)_ application can be installed by doing:

```
git clone https://gitlab.com/dgh1/nodejs-rest-web-app.git
```
Follow all the instructions in the README file to get that application running.

**NOTE:**  This  _(front-end)_  application is already installed as part of that 
_(back-end)_ application. The README file explains how to launch it.

Now for this application the following:

## Available Scripts

* yarn run build
* yarn run lint
* yarn run test
* yarn run coverage

## Install Application
```
git clone  https://gitlab.com/dgh1/react-nodejs-crud-app.git
```
Then cd into the project and do:

```
yarn  install
```

## Running the Application

First build the application.
```
yarn run build
```
The build will be in ./public.

Assuming you have something like http-server tool, if not you can 
easily install this.

```
npm install http-server -g
```

Then cd to the ./public directory and do:

```
http-server
```

You should see something like:

```
Starting up http-server, serving ./
Available on:
  http://127.0.0.1:8080
  http://192.168.1.27:8080
Hit CTRL-C to stop the server
```


Then in your browser:
```
http://localhost:8080/
```

If you have not installed the back-end application, the API will fail. You should
see an error message "Status: 503 Service Unavailable" under the 
people table.

The LOGIN button is mute, no real login is require just enter some data for 
name and password. 
~~~
NOTE: this is in clear text so don't enter any favorite password that
you would not want to be potentially compromised.
~~~ 
 Notice the button on the left of header
changes its state after clicking "sign in" from the  login GUI. Meaning, now if you click
it a drop menu shows.

Finally, as a debug aid you can see the last 30 redux state changes in
the console window by doing:

```
console.log(window.stateLogger)
```

## Running the Application in HTTPS mode

_*To run this app in HTTPS mode, change src/common/common.js as per comments in that file.*_


NOTE:  Browser testing was only done on 
Goggle Chrome and FireFox



